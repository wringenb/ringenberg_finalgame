﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

    public int startingHealth = 50;
    public int currentHealth;
    public float sinkSpeed = 2.5f;
    public AudioClip deathClip;

    Animator anim;
    bool isDead;
    bool isSinking;
    AudioSource enemyAudio;
    ScoreManager score;

    // Use this for initialization
    void Awake() {
        currentHealth = startingHealth;
        enemyAudio = GetComponent <AudioSource> ();
    }

    // Update is called once per frame
    void Update() {
        if (isSinking)
        {
            transform.Translate(-Vector3.up * sinkSpeed * Time.deltaTime);
        }
    }

    public void TakeDamage(int amount)
    {
        if (isDead)
            return;

        currentHealth -= amount;

        if (currentHealth <= 0)
        {
            Death();
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("fire"))
        {
            currentHealth -= 50;
        }

        if (currentHealth <= 0 && !isDead)
        {
            ScoreManager.score += 10;
            Death();
        }
    }
    void Death()
    {
        isDead = true;

        Destroy(gameObject);
    }
}